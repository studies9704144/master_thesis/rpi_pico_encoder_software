#include <stdio.h>
#include "pico/stdlib.h"
#include "string.h"
#include "hardware/irq.h"

#define ENCODER_INPUT_A 12
#define ENCODER_INPUT_B 13

enum state
{
    LEVEL_LOW = 0x1,  // 0x1
    LEVEL_HIGH = 0x2, // 0x2
    EDGE_FALL = 0x4,  // 0x4
    EDGE_RISE = 0x8   // 0x8
};

static volatile int64_t k_current_time = 0;
static volatile int64_t k_prev_time = 0;

static volatile bool direction = true;
static volatile uint8_t k_current_phase = 0;
static volatile uint8_t k_prev_phase = 0;

static const uint8_t k_forward_direction[] = {0b00, 0b01, 0b11, 0b10, 0b00};
static const uint8_t k_reverse_direction[] = {0b00, 0b10, 0b11, 0b01, 0b00};

void gpio_callback(uint gpio, uint32_t events);

int main()
{
    stdio_init_all();
    setup_default_uart();
    gpio_init(ENCODER_INPUT_A);
    gpio_init(ENCODER_INPUT_B);

    gpio_set_dir(ENCODER_INPUT_A, GPIO_IN); // Set the pin as an input
    gpio_set_dir(ENCODER_INPUT_B, GPIO_IN); // Set the pin as an input

    gpio_set_irq_enabled_with_callback(ENCODER_INPUT_A, GPIO_IRQ_EDGE_RISE, true, &gpio_callback);
    gpio_set_irq_enabled_with_callback(ENCODER_INPUT_B, GPIO_IRQ_EDGE_RISE, true, &gpio_callback);
    gpio_set_irq_enabled_with_callback(ENCODER_INPUT_A, GPIO_IRQ_EDGE_FALL, true, &gpio_callback);
    gpio_set_irq_enabled_with_callback(ENCODER_INPUT_B, GPIO_IRQ_EDGE_FALL, true, &gpio_callback);

    while (true)
    {
        sleep_ms(250);

        for (int i = 1; i < 5; i++)
        {
            if (k_forward_direction[i - 1] == k_current_phase && k_forward_direction[i] == k_prev_phase)
            {
                direction = true;
                break;
            }
            else if (k_reverse_direction[i - 1] == k_current_phase && k_reverse_direction[i] == k_prev_phase)
            {
                direction = false;
                break;
            }
        }

        printf("Direction: %b, time: %lld  \r\n", direction, (k_current_time - k_prev_time));
    }
}

void gpio_callback(uint gpio, uint32_t events)
{
    k_prev_time = k_current_time;
    k_current_time = time_us_64();
    k_prev_phase = k_current_phase;

    switch (gpio)
    {
    case ENCODER_INPUT_A:
        switch (events)
        {
        case EDGE_FALL:
            if (gpio_get(ENCODER_INPUT_B))
            {
                k_current_phase = 0b01;
            }
            else
            {
                k_current_phase = 0b00;
            }
            break;

        case EDGE_RISE:
            if (gpio_get(ENCODER_INPUT_B))
            {
                k_current_phase = 0b11;
            }
            else
            {
                k_current_phase = 0b10;
            }
            break;
        }
        break;
    case ENCODER_INPUT_B:
        switch (events)
        {
        case EDGE_FALL:
            if (gpio_get(ENCODER_INPUT_A))
            {
                k_current_phase = 0b10;
            }
            else
            {
                k_current_phase = 0b00;
            }
            break;

        case EDGE_RISE:
            if (gpio_get(ENCODER_INPUT_A))
            {
                k_current_phase = 0b11;
            }
            else
            {
                k_current_phase = 0b01;
            }
            break;
        }
        break;
    }
}
